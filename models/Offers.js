const mongoose = require('mongoose');
var dateFormat = require('dateformat');
var now = new Date();
var date = dateFormat(now);
//Companies Schema

const  OffersScheema = mongoose.Schema({
    title:{
        type:String,
        required:true
    },
    website:{
        type:String,
        required:true
    },

    logo:{
        type:String,
        required:true

    },
    images:{
        type:String,
        required:true

    },
    latitude:{
        type:Number,
        required:true
    },
    lontitude:{
        type:Number,
        required:true
    },  
    orariPunes:{
        type:String,
        required:true
    },
    nrTel:{
        type:String,
        required:true
    },
  
    skadon:{
        type:String,
        required:true
    },
    isFav:{
        type: Boolean,
        default: false

    },
    ofertuesi:{
        type:String,
        required:true
    },
    create_date:{
        type: String,
        default: dateFormat(now, "dddd, mmmm dS, yyyy, h:MM:ss TT")+""
    },
    date:{
        type:Date,
        default:Date.now
    }

});

 mongoose.model('Offers',OffersScheema);