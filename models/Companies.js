const mongoose = require('mongoose');


//Companies Schema

const  CompaniesScheema = mongoose.Schema({

    name:{
        type:String,
        required:true
    },
    website:{
        type:String,
        required:true
    },
    orariPunes:{
        type:String,
        required:true
    },
    
    latitude:{
        type:String,
        required:true
    },
    lontitude:{
        type:String,
        required:true
    },
    country:{
        type:String,
        required:false
    },
    logo:{
        type:String,
        required:false
    },
    create_date:{
        type:Date,
        default:Date.now
    }

});

 mongoose.model('Companies',CompaniesScheema);