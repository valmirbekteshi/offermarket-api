const mongoose = require('mongoose');


//Categories Schema
const  CategoriesScheema = mongoose.Schema({

    name:{
        type:String,
        required:true
    },
    icon:{
        type:String,
        required:true
    },

    create_date:{
        type:Date,
        default:Date.now
    }

});

 mongoose.model('Categories',CategoriesScheema);