
    const express = require('express');
const path = require('path');  
const mongoose = require('mongoose');
const exphbs = require('express-handlebars');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const firebase = require('./firebase.js')
var dateFormat = require('dateformat');
var now = new Date();





mongoose.Promise = global.Promise;
// mongodb://localhost/blejktudb

// mongodb://valmirb:38A74832a5@ds217138.mlab.com:17138/offermarket

mongoose.connect('mongodb://valmirb:38A74832a5@ds217138.mlab.com:17138/offermarket', {
        useMongoClient: true
    })
    .then(() => console.log('Mongodb is connected'))
    .catch(err => console.log(err));



//Init app
const app = express();



//Bring in models 
require('./models/Companies');
const Companies = mongoose.model('Companies');

//Offers Model
require('./models/Offers');
const Offers = mongoose.model('Offers');

//Categories Model
require('./models/Categories');
const Categories = mongoose.model('Categories');

//Load View Engine
app.engine('handlebars', exphbs({
    defaultLayout: 'main'
}));
app.set('view engine', 'handlebars');

//BodyParser Middleware
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());


//Method override middleware
app.use(methodOverride('_method'));

//Home Route
app.get('/', function (req, res) {
    res.render('index', {
        title: 'Home'
    });
});




//Companies
app.get('/companies', (req, res) => {
    Companies.find({})
        .sort({
            create_date: 'desc'
        })
        .then(companies => {
            res.render('./companies', {
                companies: companies,
                title: 'Companies'
            });
        });
});

app.get('/companies/add', (req, res) => {
    res.render('companies/add');
});


app.post('/companies', (req, res) => {
    let errors = [];
    if (!req.body.name) {
        errors.push({
            text: 'Please add a name'
        });
    }
    if (!req.body.website) {
        errors.push({
            text: 'Please add a website'
        });
    }

    if (errors.length > 0) {
        res.render('./companies/add', {
            errors: errors,
            name: req.body.name,
            website: req.body.website
        });
    } else {
        const newUser = {
            name: req.body.name,
            website: req.body.website,
            orariPunes: req.body.orariPunes,
            latitude: req.body.latitude,
            lontitude: req.body.lontitude,
            country: req.body.country,
            logo: req.body.logo
        }

        new Companies(newUser)
            .save()
            .then(companies => {
                res.redirect('/companies');
            })
    }
});

app.get('/api/companies', (req, res) => {
    Companies.find({})
        .sort({
            create_date: 'desc'
        })
        .then(companies => {
            res.json(companies);
        });
});

app.get('/api/companies/:id',(req,res) => {
    Companies.findOne({
        _id: req.params.id
    })
    .then(companies => {
        res.json(companies);
    });
});


app.get('/offers/add', (req, res) => {
    res.render('offers/add');
});

app.post('/offers', (req, res) => {

//     Categories.
//   findAll({ }).
//   populate('Categories','name').
//   exec(function (err, story) {
//     if (err) return handleError(err);
//     //console.log('The author is %s', story.author.name);
//     // prints "The author is Ian Fleming"
//   });


    let errors = [];
    if (!req.body.title) {
        errors.push({
            text: 'Please add a title'
        });
    }
    if (!req.body.website) {
        errors.push({
            text: 'Please add a website'
        });
    }
    if (!req.body.logo) {
        errors.push({
            text: 'Please add a logo'
        });
    }
    if (!req.body.images) {
        errors.push({
            text: 'Please add a image'
        });
    }

    if (errors.length > 0) {
        res.render('./offers/add', {
            errors: errors,
            title: req.body.title,
            categories:req.body.categoryId,
            website: req.body.website,
            images: req.body.images,
            ofertuesi:req.body.ofertuesi,
            logo: req.body.logo
        });
    } else {
        const newUser = {
            title: req.body.title,
            website: req.body.website,
            images: req.body.images ,
            logo: req.body.logo,
            //categories:req.body.categoryId,
            latitude:req.body.latitude,
            lontitude:req.body.lontitude,
            orariPunes:req.body.orariPunes,
            nrTel:req.body.nrTel,
            ofertuesi:req.body.ofertuesi,
            skadon:req.body.skadon
          
           

        }

        new Offers(newUser)
            .save()
            .then(offers => {
                res.redirect('/offers');
                // res.render('/offers', {
                //     categories:Categories
                // });
            })

            var title = req.body.title;
            var offer_id = req.body._id;
            var registrationToken = "/topics/all";
            
            var message = {  
                to: "/topics/all", 
                priority : 'normal',  
                body: {
                    title: req.body._id, 
                    message: 'Test',
                    offer_id: req.body._id
                } }

           firebase.fcm.send(message, function(err, response){
                if (err) {
                    console.log("Can't send FCM Masasessage.");
                } else {
                    console.log("Successfully sent with responsasse: ", response);
                }
            });

    }
});

//Offers
app.get('/offers', function (req, res) {

    Offers.find({})
        .sort({
            date: 'desc'
        })
        .then(offers => {
            res.render('./offers', {
                offers: offers,
                title: 'Offers'
            });
        });
});

app.get('/api/offers', (req, res) => {
    Offers.find({})
        .sort({
            date: 'desc'
        })
        .then(offers => {
            res.json(offers);
    });
});

app.get('/api/offers/:id',(req,res) => {
    Offers.findOne({
        _id: req.params.id
    })
    .then(offers => {
        res.json(offers);
    });
});

//post offer =======================================================

app.post('/api/offers/:id',(req,res) => {
    Offers.findOne({
        _id: req.params.id
    })
    .then(offers => {
        res.json(offers);
    });
});


app.get('/api/getOfferByBrand/:query',(req,res) => {
    Offers.find({ 
        ofertuesi: req.params.query })
    .then(offers => {
        res.json(offers);
    });
});




app.get('/api/getOfferByBrands/:query',(req,res) => {
    Offers.find({ 
        ofertuesi: req.params.query })
    .then(offers => {
        res.json(offers);
    });
});


//Categories
app.get('/categories', (req, res) => {

    Categories.find({})
        .sort({
            create_date: 'desc'
        })
        .then(categories => {
            res.render('./categories', {
                categories: categories,
                title: 'Categories'
            });
        });
});

app.get('/getJson', function (req, res) {
    // If it's not showing up, just use req.body to see what is actually being passed.
    console.log(req.body.selectpicker);
});


app.get('/api/getOfferByCategory/:query',(req,res) => {
    Offers.find({ 
        ofertuesi: req.params.query })
    .then(offers => {
        res.json(offers);
    });
});

app.get('/categories/add', (req, res) => {
    res.render('categories/add');
});

app.post('/categories', (req, res) => {
    let errors = [];

    var name = req.body.name;

    if (!req.body.name) {
        errors.push({
            text: 'Please add a name'
        });
    }

    if (errors.length > 0) {
        res.render('./categories/add', {
            errors: errors,
            name: req.body.name
        });
    } else {
        const newUser = {
            name: req.body.name,
            icon: req.body.icon
        }
        new Categories(newUser)
            .save()
            .then(categories => {
                res.redirect('/categories');
            })

           
    }
});

//EDIT
app.get('/offers/edit/:id', (req, res) => {
    Offers.findOne({
            _id: req.params.id
        })
        .then(offers => {
            res.render('offers/edit', {
                offers: offers,
                title: 'Offers | Edit'
            });
        });

});

app.put('/offers/:id', (req, res) => {
    Offers.findOne({
            _id: req.params.id
        })
        .then(offers => {

            //new value
            offers.title = req.body.title;
            offers.website = req.body.website;
            offers.pdf = req.body.website;
            offers.skadon = req.body.skadon;
            offers.logo = req.body.logo;
            offers.pdf = req.body.pdf;

            
 
            offers.save()
                .then(offers => {
                    res.redirect('/offers');
                });
        })
});

app.get('/categories/edit/:id', (req, res) => {
    Categories.findOne({
            _id: req.params.id
        })
        .then(categories => {
            res.render('categories/edit', {
                categories: categories
            });
        });

});

app.put('/categories/:id', (req, res) => {
    Categories.findOne({
            _id: req.params.id
        })
        .then(categories => {
            //new value
            categories.name = req.body.name;
            categories.icon = req.body.icon;

            categories.save()
                .then(categories => {
                    res.redirect('/categories');
                });
        })
});


app.get('/companies/edit/:id', (req, res) => {
    Companies.findOne({
            _id: req.params.id
        })
        .then(companies => {
            res.render('companies/edit', {
                companies: companies
            });
        });

});



app.put('/companies/:id', (req, res) => {
    Companies.findOne({
            _id: req.params.id
        })
        .then(companies => {
            //new value
            companies.name = req.body.name;
            companies.website = req.body.website;
            companies.country = req.body.country;
            companies.orariPunes = req.body.orariPunes;
            companies.latitude = req.body.latitude;
            companies.lontitude= req.body.lontitude;
            companies.country = req.body.country;
            companies.logo= req.body.logo;

            companies.save()
                .then(companies => {
                    res.redirect('/companies');
                });
        })
});


app.use('/api/categories', (req, res) => {
    Categories.find({})
        .sort({
            create_date: 'desc'
        })
        .then(categories => {
            res.json(categories);
        });
});



//DELETE

app.delete('/companies/:id', (req, res) => {
    Companies.remove({
            _id: req.params.id
        })
        .then(() => {
            res.redirect('/companies');
        })
});

app.delete('/categories/:id', (req, res) => {
    Categories.remove({
            _id: req.params.id
        })
        .then(() => {
            res.redirect('/categories');
        })
});


app.delete('/offers/:id', (req, res) => {
    Offers.remove({
            _id: req.params.id
        })
        .then(() => {
            res.redirect('/offers');

        })
});

app.listen(process.env.PORT || 3000, function () {
    console.log('server started on port 3000');
});

